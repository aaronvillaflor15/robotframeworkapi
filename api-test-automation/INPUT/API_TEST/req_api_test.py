REQ_POST_API_TEST = {
        "id": 11,
        "name": "Aaron Villaflor",
        "username": "Aaron_Villaflor",
        "email": "aaronvillaflor@test.me",
        "address": {
            "street": "Greenwood Heights",
            "suite": "Suite 729",
            "city": "Dasmarinas",
            "zipcode": "4114",
            "geo": {
                "lat": "-14.3990",
                "lng": "-120.7677"
            }
        },
        "phone": "9392433235",
        "website": "none",
        "company": {
            "name": "Datacom",
            "catchPhrase": "Implemented secondary concept",
            "bs": "e-enable extensible e-tailers"
        }
    }

